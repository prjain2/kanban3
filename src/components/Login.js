import React from 'react'
import './css/login.css'
const Login=()=>{
    function add(){
        console.log('hi');
    }
  return(
    <>
   <div class="main">
        <div class="navbar">
            <div class="icon">
                <h2 id="title"class="logo">Todo</h2>
            </div>
        </div> 
       <div class="content">
            <h1>Welcome to do  <br/><span>task</span> <br/></h1>
            <p class="par"> Todo involves creating a tool for
                managing tasks and increasing productivity & <br/>
                To create a user-friendly and intuitive interface for users to
                create, organize,<br/> and prioritize tasks in order to stay on top of their
                workload and meet their goals.</p>
                
              <div class="form"> 
                    <h2>Login Here</h2>
                    <input type="email" name="email" placeholder="Enter Email Here"/>
                    <input type="password" name="" placeholder="Enter Password Here"/>
                    <button className="btnn" onClick={add}><a href="/board">Login</a></button>

                    <p class="link">Don't have an account<br/>
                    <a href="/Register">Sign up here</a></p>
                    <p class="liw">Log in with</p>

                    <div class="icons">
                        <a href="/board"><ion-icon name="logo-google">G</ion-icon></a>
                    </div>

                </div>
        </div>
    </div>
    
        </>
  )
}

export default Login