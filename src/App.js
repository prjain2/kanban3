import React from 'react'
import Login from './components/Login';
import Register from './components/Register';
import { Route } from 'react-router-dom';
import {  Routes} from 'react-router-dom';
import Dashboard from './components/board/App'
const App=()=>{
  return(
<> 
    <Routes>
    <Route path="/" element ={<Login />}/>
    <Route path="/Register" element ={<Register/>}/>
    <Route path="/board" element ={<Dashboard/>}/>
    </Routes>
</>
    
  )
}

export default App;